/* eslint-disable */
import fetch from 'node-fetch';
import supertest from 'supertest';
import { decorateService } from '../../lib/decorate';
import { urls } from '../config/index';
import { keys } from '../config/index';


export class MailBox {
  async mailBoxChecking(key, email) {
    const r = await fetch(`${urls.mailboxlayer}/check?access_key=${key}&email=${email}`, {
      method: 'post',
    });
    return r;
  };
};
