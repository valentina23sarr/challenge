/* eslint-disable */
const emails = {
    email1: 'my.usera@gmail.com',
    emailNull: '',
    emailWithDot: '.my.usera@gnail.com',
    emailWithTypo: 'my.usera@gnail.com',
    emailWith2Ats: 'my.user@my@dd.com',
    email5: 'useruseruseruseruseruseruseruseruseruseruseruseruseruseruseruse64@gmail.com',
    email6: 'user@useruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruseruserus253',
  };
export { emails };